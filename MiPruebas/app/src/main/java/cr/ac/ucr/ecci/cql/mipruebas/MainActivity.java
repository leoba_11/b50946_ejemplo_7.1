package cr.ac.ucr.ecci.cql.mipruebas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView nombre;
    private TextView identificacion;
    private Button buttonLimpiar;
    private Button buttonCargar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        nombre = (TextView) findViewById(R.id.nombre);
        identificacion = (TextView) findViewById(R.id.identificacion);
        buttonLimpiar = (Button) findViewById(R.id.buttonLimpiar);
        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                limpiar();
            }
        });
        buttonCargar = (Button) findViewById(R.id.buttonCargar);
        buttonCargar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                cargar();
            }
        });
        cargar();
    }

    public void limpiar() {
        nombre.setText("");
        identificacion.setText("");
    }

    public void cargar() {
        DataBaseDataSource mDataBaseDataSource = new
                DataBaseDataSource(getApplicationContext());
        Persona mPersona = mDataBaseDataSource.leerPersona("1001");
        nombre.setText(mPersona.getNombre());
        identificacion.setText(mPersona.getIdentificacion());
    }
}