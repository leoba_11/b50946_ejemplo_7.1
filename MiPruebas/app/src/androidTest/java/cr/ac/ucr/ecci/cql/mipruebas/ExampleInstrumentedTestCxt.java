package cr.ac.ucr.ecci.cql.mipruebas;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTestCxt {
    @Test
    public void testContext() throws Exception {
// Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("cr.ac.ucr.ecci.cql.mipruebas", appContext.getPackageName());
    }
}
