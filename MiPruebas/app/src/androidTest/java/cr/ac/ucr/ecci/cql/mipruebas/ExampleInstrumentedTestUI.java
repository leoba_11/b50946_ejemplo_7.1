package cr.ac.ucr.ecci.cql.mipruebas;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;


import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;


@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTestUI {
    public static final String TEST_STRING_EMPTY = "";
    public static final String TEST_STRING_ID = "1001";
    public static final String TEST_STRING_NAME = "Immanuel Kant";
    @Rule
    public ActivityTestRule mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testId() {
        onView(withId(R.id.identificacion)).check(matches(withText(TEST_STRING_ID)));
    }

    @Test
    public void testName() {
        onView(withId(R.id.nombre)).check(matches(withText(TEST_STRING_NAME)));
    }

    @Test
    public void testLimpiar() {
        onView(withId(R.id.buttonLimpiar)).perform(click());
        onView(withId(R.id.identificacion)).check(matches(withText(TEST_STRING_EMPTY)));
        onView(withId(R.id.nombre)).check(matches(withText(TEST_STRING_EMPTY)));
    }

    @Test
    public void testCargar() {
        onView(withId(R.id.buttonCargar)).perform(click());
        onView(withId(R.id.identificacion)).check(matches(withText(TEST_STRING_ID)));
        onView(withId(R.id.nombre)).check(matches(withText(TEST_STRING_NAME)));
    }
}